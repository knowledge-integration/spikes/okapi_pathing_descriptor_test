package mod.dummy

import grails.core.GrailsApplication
import grails.converters.*
import static org.springframework.http.HttpStatus.*


class TenantController {

  GrailsApplication grailsApplication

  def index() {
    log.info("TenantController::index() ${params}");
    log.info("JSON: ${request.JSON}");
    request.getHeaderNames().each {        
      log.info(it+":"+request.getHeader(it))
    }
    def data = [ test: 'wibble' ]
    render ([status: 200], (data ?: [:]) as JSON)
  }

  def disable() {
    log.info("TenantController::index() ${params}");
    request.getHeaderNames().each {        
      log.info(it+":"+request.getHeader(it))
    }
    def data = null;
    render ( [status: NO_CONTENT.value()], (data ?: [:]) as JSON )
  }
}

