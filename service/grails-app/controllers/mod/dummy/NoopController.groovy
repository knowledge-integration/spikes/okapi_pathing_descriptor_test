package mod.dummy

import grails.core.GrailsApplication
import grails.converters.*
import static org.springframework.http.HttpStatus.*


class NoopController {

  GrailsApplication grailsApplication

  def index() {
    log.info("NoopController::index() ${params}");
    log.info("JSON: ${request.JSON}");
    request.getHeaderNames().each {        
      log.info(it+":"+request.getHeader(it))
    }
    def data = [ test: 'wibble' ]
    render ([status: 200], (data ?: [:]) as JSON)
  }
}

