package mod.dummy

class UrlMappings {

  static mappings = {

    group '/dummy/_', {
      // Tenant mappings.
      '/tenant' (controller: 'tenant', method: 'POST')
      '/tenant' (controller: 'tenant', method: 'DELETE')
      '/tenant/disable' (controller: 'tenant', action: 'disable', method: 'POST')
    }

    '/dummy/dummy/noop' (controller: 'noop')
  }
}
