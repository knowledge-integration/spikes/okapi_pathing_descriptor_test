


Run app from service directory

  grails run-app

Check tenant endpoints:

  curl -X POST "http://localhost:8080/dummy/_/tenant"
  curl -X DELETE "http://localhost:8080/dummy/_/tenant"
  curl -X POST "http://localhost:8080/dummy/_/tenant/disable"


Spin up a FOLIO system from the root of the project

  vagrant up


Install and enable

  ./register_and_enable.sh

Test a command

  ./okapi-cmd /dummy/noop
